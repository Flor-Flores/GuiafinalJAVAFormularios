package clases;

/**
 *Nombre de la clase:Carrera
 * fecha:25-05-18
 * Version: 1.0
 * Copyrigth: Flor Flores
 * @author Flor Flores
 */
public class Carrera {
    private String facultad;
    private String nombre;
    private String descripcion;

    public Carrera() {
    }
    public Carrera(String facultad, String nombre, String descripcion) {
        this.facultad = facultad;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getFacultad() {
        return facultad;
    }
    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
