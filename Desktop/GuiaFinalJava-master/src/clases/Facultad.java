package clases;

/**Nombre de la clase:Facultad
 * fecha:25-05-18
 * Version: 1.0
 * Copyrigth: Flor Flores
 * @author Flor Flores
 */
public class Facultad {
    private String universidad="Tecnologica";
    private String sede="San Salvador";
    private int numCarreras=10;

    public Facultad() {
    }
    public String getUniversidad() {
        return universidad;
    }
    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }
    public String getSede() {
        return sede;
    }
    public void setSede(String sede) {
        this.sede = sede;
    }
    public int getNumCarreras() {
        return numCarreras;
    }
    public void setNumCarreras(int numCarreras) {
        this.numCarreras = numCarreras;
    }
    
    
}
