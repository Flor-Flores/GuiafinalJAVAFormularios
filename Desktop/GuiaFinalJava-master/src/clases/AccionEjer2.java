package clases;

/* * *Nombre de la clase:AccionEjer2
 * fecha:24-05-18
 * Version: 1.0
 * Copyrigth: Flor Flores
 * @author Flor Flores
 */
public interface AccionEjer2 {
    public void calcularArea(String tipo);
    public void mostrarResultado();
}
