package clases;

import javax.swing.JOptionPane;

/**
 * *Nombre de la clase:Ejer2
 * fecha:24-05-18
 * Version: 1.0
 * Copyrigth: Flor Flores
 * @author Flor Flores
 */
 
public class Ejer2 implements AccionEjer2 {
    private double valor1=0.0;
    private double valor2=0.0;
    private static double area=0;
   
    @Override
    public void calcularArea(String tipo){
        switch(tipo){
           
              case "triang":
               area = (this.valor1 * this.valor2)/2;
                JOptionPane.showMessageDialog(null, "El area del Triangulo es: "+area);
                break;
            case "rectan":
               area = this.valor1 * this.valor2;
                JOptionPane.showMessageDialog(null, "El area del Rectangulo es: "+area);
                break;
           case "romboi":
               area = this.valor1 * this.valor2;
                JOptionPane.showMessageDialog(null, "El area del Romboide es: "+area);
                break;
            case "rombo":
                area =(this.valor1 * this.valor2)/2;
                JOptionPane.showMessageDialog(null, "El area del Rombo es: "+area);
                break;
          
        }
    }
     @Override
     public void mostrarResultado(){
         
     }
    public double getValor1() {
        return valor1;
    }
    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }
    public double getValor2() {
        return valor2;
    }
    public void setValor2(double valor2) {
        this.valor2 = valor2;
    }
    
}
