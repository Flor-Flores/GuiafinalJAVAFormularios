
package clases;

import javax.swing.JOptionPane;

/**
 * Nombre de la clase:Ejer7
 * fecha:25-05-18
 * Version: 1.0
 * Copyrigth: Flor Flores
 * @author Flor Flores
 */
 
public class Ejer7 {
    private double num1;
    private double num2;
    public Ejer7() {
    }

    public double getNum1() {
        return num1;
    }
    public void setNum1(double num1) {
        this.num1 = num1;
    }
    public double getNum2() {
        return num2;
    }
    public void setNum2(double num2) {
        this.num2 = num2;
    }
    
    public void sumar(float num1, float num2){
        float suma=0;        
        suma = num1 + num2;
        JOptionPane.showMessageDialog(null, "La suma es: "+suma);
    }
    public void restar(float num1, float num2){
        float resta=0;
        resta = num1 - num2;
        JOptionPane.showMessageDialog(null, "La Restar es: "+resta);
    }
    public void multiplicar(float num1, float num2){
        float multi=0;
        multi = num1 * num2;
        JOptionPane.showMessageDialog(null, "La Multiplicacion es: "+multi);
    }
    public void dividir(float num1, float num2){
        float dividir=0;
        dividir = num1 / num2;
        JOptionPane.showMessageDialog(null, "La divicion es: "+dividir);
    }
    public void potencia(double num1, double num2){
        double pow=0;
        pow = Math.pow(num1, num2);
        JOptionPane.showMessageDialog(null, "La Potencia es: "+pow);
    }
    public void sqrt(double num1, double num2){
     double sqrt=0;
     sqrt = Math.pow(num1,1/num2);
     JOptionPane.showMessageDialog(null, "La Raiz "+num2+" de " +num1 + " es: "+sqrt);
    }
}
