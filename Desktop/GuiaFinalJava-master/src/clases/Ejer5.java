
package clases;

/**
 * Nombre de la clase:Ejer5
 * fecha:24-05-18
 * Version: 1.0
 * Copyrigth: Flor Flores
 * @author Flor Flores
 */
public class Ejer5 {
   
    private String nombre;
    private String carnet;
    private double promeTeoria;
    private double promePractica;
    private double promeFinal;

    public Ejer5() {
    }

    public Ejer5(String nombre, String carnet, double promeTeoria, double promePractica, double promeFinal) {
        this.nombre = nombre;
        this.carnet = carnet;
        this.promeTeoria = promeTeoria;
        this.promePractica = promePractica;
        this.promeFinal = promeFinal;
    }

    public double getPromeFinal() {
        return promeFinal;
    }

    public void setPromeFinal(double promeFinal) {
        this.promeFinal = promeFinal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    public double getPromeTeoria() {
        return promeTeoria;
    }

    public void setPromeTeoria(double promeTeoria) {
        this.promeTeoria = promeTeoria;
    }

    public double getPromePractica() {
        return promePractica;
    }

    public void setPromePractica(double promePractica) {
        this.promePractica = promePractica;
    }
    
}
